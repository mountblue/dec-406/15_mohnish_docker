# Use an official Python runtime as a parent image
FROM python:3.6

ENV PYTHONUNBUFFERED 1

COPY . /ipl

# Set the working directory to /ipl
WORKDIR /ipl

ADD /config/requirements.txt /config/

# Install any needed packages specified in requirements.txt
RUN pip install -r /config/requirements.txt


